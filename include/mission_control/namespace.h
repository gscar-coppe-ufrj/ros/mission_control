/*!
 * \file namespace.h
 */



#ifndef MISSION_CONTROL_NAMESPACE_H
#define MISSION_CONTROL_NAMESPACE_H



/*!
 * \namespace mission_control
 * \brief This namespace contains classes responsible for creating missions and
 *      missions plans and managing them.
 */
namespace mission_control {
}



#endif


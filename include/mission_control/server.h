/*!
 * \file server.h
 */
 
 
 
#ifndef MISSION_CONTROL_SERVER
#define MISSION_CONTROL_SERVER



#include <ctime>
#include <boost/thread.hpp>

#include <mission_control/mission_plan_manager.h>
#include <mission_control/schedule_manager.h>

#include <mission_control/MissionPlan.h>
#include <mission_control/ScheduleInfo.h>
#include <mission_control/BufferMission.h>



namespace mission_control {



/*!
 * \brief The Server class initializes all the services of MissionControl package.
 */
class Server
{
    enum ScheduleType {
        SingleSchedule = 0,
        TimedSchedule = 1,
        RecurringSchedule = 2
    };

    /// Internal cycle for update the buffer.
    boost::thread* cycleThread;

    /// 
    bool cycleOn;

    /*!
     * \brief GetScheduleRequestTime is used to compute the next time an
     *      single schedule is going to be requested, based on the current time.
     * \param schedule The schedule to compute the next request time.
     * \return A time_t with the next request time.
     */
    time_t ComputeSingleScheduleRequestTime(const ScheduleInfo& schedule);
    /*!
     * \brief GetScheduleRequestTime is used to compute the next time a timed
     *      schedule is going to be requested, based on the current time.
     * \param schedule The schedule to compute the next request time.
     * \return A time_t with the next request time.
     */
    time_t ComputeTimedScheduleRequestTime(const ScheduleInfo& schedule);
    /*!
     * \brief GetScheduleRequestTime is used to compute the next time a
     *      recurring schedule is going to be requested, based on the current
     *      time.
     * \param schedule The schedule to compute the next request time.
     * \return A time_t with the next request time.
     */
    time_t ComputeRecurringScheduleRequestTime(const ScheduleInfo& schedule);

    /*!
     * \brief AddNextSchedule checks if its time to add a mission from a
     *      schedule in to the buffer.
     * \return Returns 0 if the schedule added has finished, 1 if there a more
     *      occurrences and -1 if nothing was added.
     */
    int AddNextSchedule();

    /*!
     * \brief AddSingleSchedule adds a single time schedule to the buffer.
     * \param id Position of the schedule in the list.
     * \return Always return 0.
     */
    int AddSingleSchedule(int position);
    /*!
     * \brief AddTimedSchedule adds a periodic schedule to the buffer.
     * \param id Position of the schedule in the list.
     * \return Returns 0 if the schedule is over, 1 otherwise.
     */
    int AddTimedSchedule(int position);

    void UpdateBuffer();

protected:
    /// Mutex for the internal cycle.
    boost::mutex mut;
    boost::condition_variable cond;

    boost::mutex mutPlan;
    boost::mutex mutSched;
    boost::mutex mutBuffer;

    ///
    bool playState;
    bool stateMachineStatus;

    /// Object used to add, remove and edit the mission plan xml file.
    MissionPlanManager mpman;
    /// Object used to add, remove and edit the schedule xml file.
    ScheduleManager msched;

    /// Actual mission plan being executed.
    BufferMission actualMission;
    /// Mission buffer.
    std::vector<BufferMission> missionBuffer;
    /// List with the execution times of the schedules.
    std::vector<time_t> scheduleTimes;

    /// Pointer to the list of mission plans.
    const std::vector<MissionPlan>* mpPointer;
    /// Pointer to the list of schedules.
    const std::vector<ScheduleInfo>* schedPointer;

public:
    /*!
     * \brief Server 
     */
    Server();
    /*!
     * \brief ~Server saves the lists of mission plans and schedules to their
     *      respective xml file.
     */
    ~Server();
  
    /*!
     * \brief GetScheduleRequestTime is used to compute the next time a schedule
     *      is going to be requested, based on the current time.
     * \param schedule The schedule to compute the next request time.
     * \return A time_t with the next request time.
     */
    time_t ComputeScheduleRequestTime(const ScheduleInfo& schedule);

    static void Cycle(Server* server);

    void StartCycle();
    void StopCycle();

    virtual void SendMission() {}

    virtual void NotifyBufferUpdate(BufferMission missionPlan) {}
    virtual void NotifyScheduleUpdate(ScheduleInfo schedule, int type) {}
};



}



#endif

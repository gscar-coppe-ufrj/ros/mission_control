/*!
 * \file services.h
 */



#ifndef MISSION_CONTROL_SERVICES
#define MISSION_CONTRL_SERVICES



/* MissionPlanManager Services */

#include <mission_control/AddMissionPlan.h>
#include <mission_control/RemoveMissionPlan.h>
#include <mission_control/MissionPlanList.h>

/* End of MissionPlanManager Services */

/* ScheduleManager Services */

#include <mission_control/AddMissionPlanSchedule.h>
#include <mission_control/RemoveMissionPlanSchedule.h>
#include <mission_control/ScheduleList.h>

/* End of ScheduleManager Services */

/* Server Services */

// Execution control services
#include <mission_control/GetCurrentMission.h>
#include <mission_control/SetCurrentMission.h>
#include <mission_control/CancelCurrentMission.h>

// Buffer control services
#include <mission_control/AddMissionToBuffer.h>
#include <mission_control/MoveMissionOnBuffer.h>
#include <mission_control/RemoveMissionFromBuffer.h>
#include <mission_control/BufferControl.h>
#include <mission_control/MissionBufferList.h>

/* End of Server Services */



#endif

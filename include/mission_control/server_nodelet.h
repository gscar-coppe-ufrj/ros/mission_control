/*!
 * \file server_nodelet.h
 */



#ifndef MISSION_CONTROL_SERVER_NODELET
#define MISSION_CONTROL_SERVER_NODELET



#include <ros/ros.h>
#include <nodelet/nodelet.h>

#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

#include <mission_control/server.h>
#include <mission_control/mission_plan_serialization.h>

#include <mission_control/services.h>

#include <logger/logger.h>



namespace mission_control {



class ServerNodelet : public nodelet::Nodelet, mission_control::Server, logger::Logger
{
    enum ShiftOrder {
        UP = true,
        DOWN = false
    };

    enum BufferFlag {
        Play = 0,
        Pause = 1,
        Stop = 2
    };

    enum ActionType {
        Add = 0,
        Remove = 1,
        Execute = 2,
        Move = 3
    };

    /// Class used to generate the messages.
    MissionPlanSerialization mpserial;

    /// Path of the xml file that contains the mission plans.
    std::string missionPath;
    /// Path of the xml file that contains the schedules.
    std::string schedulePath;

    /// ROS node.
    ros::NodeHandle nodehandle;

    /* ROS Subscribers and Publishers */

    ///
    ros::Subscriber missionStatusSub;

    ///
    ros::Publisher missionPlanPub;
    /// Play, pause and stop.
    ros::Publisher missionControlPub;

    ros::Publisher missionPlanUpdatePub;
    ros::Publisher missionSchedUpdatePub;
    ros::Publisher missionBufferUpdatePub;

    ros::Publisher bufferStatusUpdatePub;
    ros::Publisher actualMissionUpdatePub;
    
    /* End of ROS Subscribers and Publishers */

    /* MissionPlanManager Services */
    
    /// Add a mission plan to the list.
    ros::ServiceServer srvAddMissionPlan;
    /// Remove a mission plan from the list.
    ros::ServiceServer srvRemoveMissionPlan;
    /// Not implemented yet.
    // What?
    ros::ServiceServer srvSetMissionPlanState;
    /// List all the mission plans.
    ros::ServiceServer srvMissionPlanList;
    /// Not implemented yet.
    // What?²
    ros::ServiceServer srvMissionPlanBuffer;
    
    /* End of MissionPlanManager Services */
    
    /* ScheduleManager Services */
    
    /// Add a schedule to the list.
    ros::ServiceServer srvAddMissionPlanSchedule;
    /// Remove a schedule from the list.
    ros::ServiceServer srvRemoveMissionPlanSchedule;
    /// List all the schedules.
    ros::ServiceServer srvScheduleList;
    
    /* End of ScheduleManager Services */    

    /* Server Services */

    ros::ServiceServer srvGetCurrentMission;
    ros::ServiceServer srvSetCurrentMission;
    ros::ServiceServer srvCancelCurrentMission;

    ros::ServiceServer srvAddMissionToBuffer;
    ros::ServiceServer srvMoveMissionOnBuffer;
    ros::ServiceServer srvRemoveMissionFromBuffer;
    ros::ServiceServer srvBufferControl;
    ros::ServiceServer srvMissionBufferList;

    /* End of Server Services */

    void TreatMissionPlanState(const std_msgs::Bool::ConstPtr& mp);

    bool ExecuteSpecificMission(int position);
    bool RearrangeBuffer(int position, int times, ServerNodelet::ShiftOrder shift);
    bool CancelMission(int position, int id);

public:
    ServerNodelet();
    ~ServerNodelet();
  
    /*!
     * \brief onInit is called when the nodelet is created.
     *
     * This function will get the rosparams used in roslaunch, load the xml
     *  files and set up the services of the mission control module.
     */
    virtual void onInit();

    /*!
     * \brief SendMission
     */
    virtual void SendMission();

    virtual void NotifyBufferUpdate(BufferMission missionPlan);
    virtual void NotifyScheduleUpdate(ScheduleInfo schedule, int type);

    bool GetCurrentMission(GetCurrentMission::Request& req, GetCurrentMission::Response& res);
    bool SetCurrentMission(SetCurrentMission::Request& req, SetCurrentMission::Response& res);
    bool CancelCurrentMission(CancelCurrentMission::Request& req, CancelCurrentMission::Response& res);

    bool AddMissionToBuffer(AddMissionToBuffer::Request& req, AddMissionToBuffer::Response& res);
    bool MoveMissionOnBuffer(MoveMissionOnBuffer::Request& req, MoveMissionOnBuffer::Response& res);
    bool RemoveMissionFromBuffer(RemoveMissionFromBuffer::Request& req, RemoveMissionFromBuffer::Response& res);
    bool BufferControl(BufferControl::Request& req, BufferControl::Response& res);
    bool MissionBufferList(MissionBufferList::Request& req, MissionBufferList::Response& res);

    /*!
     * \brief AddMissionPlan is the service responsible for adding a new
     *      mission plan to the missionPlan.
     * \param req Mission plan to be added.
     * \param res 1 if the mission plan was added, 0 otherwise.
     * \return Always returns true.
     */
    bool AddMissionPlan(AddMissionPlan::Request& req, AddMissionPlan::Response& res);
    /*!
     * \brief RemoveMissionPlan is the service responsible for removing a
     *      mission plan from the missionPlan.
     * \param req Id of the mission plan to be removed.
     * \param res 1 if mission plan was removed, 0 otherwise.
     * \return Always returns true.
     */
    bool RemoveMissionPlan(RemoveMissionPlan::Request& req, RemoveMissionPlan::Response& res);
    /*!
     * \brief MissionPlanList is the service for listing all the mission plans.
     * \param res List of all the actual mission plans.
     * \return Always returns true.
     */
    bool MissionPlanList(MissionPlanList::Request& req, MissionPlanList::Response& res);

    /*!
     * \brief AddMissionPlanSchedule is the service responsible for adding a
     *      new schedule to the list.
     * \param req Schedule to be added.
     * \param res 1 if the schedule was added, 0 otherwise.
     * \return Always returns true.
     */
    bool AddMissionPlanSchedule(AddMissionPlanSchedule::Request& req, AddMissionPlanSchedule::Response& res);
    /*!
     * \brief RemoveMissionPlanSchedule is the service responsible for removing
     *      a schedule from the list.
     * \param req Id from the schedule soon to be removed.
     * \param res 1 if the schedule was removed, 0 otherwise.
     * \return Always returns true.
     */
    bool RemoveMissionPlanSchedule(RemoveMissionPlanSchedule::Request& req, RemoveMissionPlanSchedule::Response& res);
    /*!
     * \brief ScheduleList is the service responsible for listing all existing
     *      schedules and their information.
     * \param resp List of all the actual schedules.
     * \return Always returns true.
     */
    bool ScheduleList(ScheduleList::Request &req, ScheduleList::Response &resp);
};



}



#endif

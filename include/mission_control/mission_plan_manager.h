/*!
 * \file mission_plan_manager.h
 */
 
 

#ifndef MISSION_CONTROL_MISSION_PLAN_MANAGER_H
#define MISSION_CONTROL_MISSION_PLAN_MANAGER_H



#include <mission_control/MissionPlan.h>
#include <mission_control/AddMissionPlan.h>
#include <mission_control/RemoveMissionPlan.h>
#include <mission_control/MissionPlanList.h>



namespace mission_control {


 
/*!
 * \class MissionPlanManager
 * \brief This class is responsible for loading, updatding and saving the
 *      mission plan list and their information.
 */
class MissionPlanManager
{
    /// This vector contains all the actual mission plans.
    std::vector<MissionPlan> missionPlan;

public:
    /*!
     * \brief MissionPlanManager does nothing.
     */
    MissionPlanManager();
    /*!
     * \brief ~MissionPlanManager does nothing.
     */
    ~MissionPlanManager();
    
    /*!
     * \brief Save saves the mission plan list into to a xml file.
     * \param filename Xml that contains the mission plan list.
     * \return Not implemented.
     */
    int Save(const char* filename);
    /*!
     * \brief Load loads the mission plan list from the xml and adds to the
     *      missionPlan.
     * \param filename Xml that contains the mission plan list.
     * \return Not implemented.
     */
    int Load(const char* filename);

    /*!
     * \brief Add adds a new mission plan to the missionPlan.
     * \param plan Mission plan to be added.
     * \return Returns 1 if sucessful, 0 otherwise.
     */    
    int Add(MissionPlan& plan);
    /*!
     * \brief Remove removes a mission plan from the missionPlan.
     * \param id Id of the mission plan to be removed.
     * \return Returns 1 if sucessful, 0 otherwise.
     */    
    int Remove(const std::string name);
    
    const std::vector<MissionPlan>* GetMissionPlanList();

    /*!
     * \brief AddMissionPlan is the service responsible for adding a new
     *      mission plan to the missionPlan.
     * \param req Mission plan to be added.
     * \param res 1 if the mission plan was added, 0 otherwise.
     * \return Always returns true.
     */
    bool AddMissionPlan(AddMissionPlan::Request& req, AddMissionPlan::Response& res);
    /*!
     * \brief RemoveMissionPlan is the service responsible for removing a
     *      mission plan from the missionPlan.
     * \param req Id of the mission plan to be removed.
     * \param res 1 if mission plan was removed, 0 otherwise.
     * \return Always returns true.
     */
    bool RemoveMissionPlan(RemoveMissionPlan::Request& req, RemoveMissionPlan::Response& res);
    /*!
     * \brief MissionPlanList is the service for listing all the mission plans.
     * \param res List of all the actual mission plans.
     * \return Always returns true.
     */
    bool MissionPlanList(MissionPlanList::Request& req, MissionPlanList::Response& res);
};



} // namespace mission_control



#endif

/*!
 * \file MissionPlanSerialization.h
 */
 
 
 
#ifndef MISSIONPLANSERIALIZATION_H
#define MISSIONPLANSERIALIZATION_H



#include <map>
#include <vector>

#include <mission_control/MissionPlan.h>



namespace mission_control {



/*!
 * \class MissionPlanSerialization
 * \brief MissionPlanSerialization
 */
class MissionPlanSerialization
{
    enum PARAMTYPE {
        Float,
        Int,
        Bool,
        String
    };
    
    typedef std::map<std::string, std::string> ParamMap;
    //ParamMap missionParamMap;
    
    class ParamTypeList
    {
        public:
            int id;
            std::vector<std::string> params;
            std::vector<PARAMTYPE> types;
    };
    
    typedef std::map<std::string, ParamTypeList> MissionDefinition;
    MissionDefinition missionDefinitions;
    
    /*!
     * \brief
     *
     * \param missionName
     * \param missionId
     * \param paramName
     * \param type
     *
     * \return A string containing the Python __repr__ of the mission plan.
     */
    void AddDefinition(const char* missionName, int missionId, const char* paramName, PARAMTYPE type);
    
public:
    MissionPlanSerialization();
    ~MissionPlanSerialization();
    
    std::string Serialize(const MissionPlan& mp);
};



};



#endif 


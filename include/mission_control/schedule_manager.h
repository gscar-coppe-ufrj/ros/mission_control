/*!
 * \file schedule_manager.h
 */



#ifndef MISSION_CONTROL_SCHEDULE_MANAGER_H
#define MISSION_CONTROL_SCHEDULE_MANAGER_H



#include <mission_control/Date.h>
#include <mission_control/ScheduleInfo.h>
#include <mission_control/ScheduleList.h>
#include <mission_control/AddMissionPlanSchedule.h>
#include <mission_control/RemoveMissionPlanSchedule.h>



namespace mission_control {



/*!
 * \class ScheduleManager
 * \brief This class is responsible for loading, updatding and saving the
 *      schedules list and their information.
 */
class ScheduleManager
{
    /// This vector contains all the actual schedules.
    std::vector<ScheduleInfo> scheduleList;

public:
    /*!
     * \brief ScheduleManager initializes the scheduleList.
     */
    ScheduleManager();
    /*!
     * \brief ~ScheduleManager does nothing.
     */
    ~ScheduleManager();

    /*!
     * \brief Save saves the list of schedules and their information into to a
     *      xml file.
     * \param filename Xml file that contains the schedules.
     * \return Not implemented.
     */
    int Save(const char* filename);
    /*!
     * \brief Load load all the schedules from a xml file and add them to the
     *      scheduleList.
     * \param filename Xml file that contains the schedules.
     * \return Not implemented.
     */
    int Load(const char* filename);

    /*!
     * \brief Add adds a new schedule to the scheduleList.
     * \param item Schedule to be added.
     * \return Returns 1 if sucessful, 0 otherwise.
     */
    int Add(ScheduleInfo& schedule);
    /*!
     * \brief Remove removes a schedule from the scheduleList.
     * \param id Id correspondent to the schedule.
     * \return Returns 1 if sucessful, 0 otherwise.
     */
    int Remove(const int id);

    const std::vector<ScheduleInfo>* GetScheduleList();

    /*!
     * \brief AddMissionPlanSchedule is the service responsible for adding a
     *      new schedule to the list.
     * \param req Schedule to be added.
     * \param res 1 if the schedule was added, 0 otherwise.
     * \return Always returns true.
     */
    bool AddMissionPlanSchedule(AddMissionPlanSchedule::Request& req, AddMissionPlanSchedule::Response& res);
    /*!
     * \brief RemoveMissionPlanSchedule is the service responsible for removing
     *      a schedule from the list.
     * \param req Id from the schedule soon to be removed.
     * \param res 1 if the schedule was removed, 0 otherwise.
     * \return Always returns true.
     */
    bool RemoveMissionPlanSchedule(RemoveMissionPlanSchedule::Request& req, RemoveMissionPlanSchedule::Response& res);
    /*!
     * \brief ScheduleList is the service responsible for listing all existing
     *      schedules and their information.
     * \param resp List of all the actual schedules.
     * \return Always returns true.
     */
    bool ScheduleList(ScheduleList::Request &req, ScheduleList::Response &resp);
};



}



#endif // MISSION_CONTROL_SCHEDULE_MANAGER_H

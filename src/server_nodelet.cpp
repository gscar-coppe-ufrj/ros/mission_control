// server_nodelet.cpp



#include <mission_control/server_nodelet.h>

#include <mission_control/MissionPlan.h>
#include <mission_control/BufferMission.h>
#include <mission_control/MissionListUpdate.h>
#include <mission_control/ScheduleListUpdate.h>
#include <mission_control/BufferMissionUpdate.h>

#include <logger/ros_logger.h>

#include <string>



using namespace std;
using namespace mission_control;



ServerNodelet::ServerNodelet()
{
    logSystem = new logger::RosLogger;
}


void ServerNodelet::onInit() 
{   
    // Get node handles 
    nodehandle = getNodeHandle();
    ros::NodeHandle& privateNH = getPrivateNodeHandle();
    
    logSystem->SetNodeName(privateNH.getNamespace());
    
    LOGGER_INFO("mission_control::Server initialized.");
    
    privateNH.param("mission_path", missionPath, string(""));
    privateNH.param("schedule_path", schedulePath, string(""));

    if (missionPath == "")
        LOGGER_FATAL("Please specify a mission plan database file (mission_path).");
    
    if (schedulePath == "")
        LOGGER_FATAL("Please specify a schedule database file (schedule_path).");

    mpman.Load(missionPath.c_str());
    msched.Load(schedulePath.c_str());

    mpPointer = mpman.GetMissionPlanList();
    schedPointer = msched.GetScheduleList();

    // Check which topics going to send the last message or not

    // Advertise services
    srvAddMissionPlan = nodehandle.advertiseService("plan/add", &ServerNodelet::AddMissionPlan, this);
    srvRemoveMissionPlan = nodehandle.advertiseService("plan/remove", &ServerNodelet::RemoveMissionPlan, this);
    srvMissionPlanList = nodehandle.advertiseService("plan/list", &ServerNodelet::MissionPlanList, this);

    srvAddMissionPlanSchedule = nodehandle.advertiseService("schedule/add", &ServerNodelet::AddMissionPlanSchedule, this);
    srvRemoveMissionPlanSchedule = nodehandle.advertiseService("schedule/remove", &ServerNodelet::RemoveMissionPlanSchedule, this);
    srvScheduleList = nodehandle.advertiseService("schedule/list", &ServerNodelet::ScheduleList, this);

    srvGetCurrentMission = nodehandle.advertiseService("current_mission/get", &ServerNodelet::GetCurrentMission, this);
    srvSetCurrentMission = nodehandle.advertiseService("current_mission/set", &ServerNodelet::SetCurrentMission, this);
    srvCancelCurrentMission = nodehandle.advertiseService("current_mission/cancel", &ServerNodelet::CancelCurrentMission, this);

    srvAddMissionToBuffer = nodehandle.advertiseService("buffer/add", &ServerNodelet::AddMissionToBuffer, this);
    srvMoveMissionOnBuffer = nodehandle.advertiseService("buffer/move", &ServerNodelet::MoveMissionOnBuffer, this);
    srvRemoveMissionFromBuffer = nodehandle.advertiseService("buffer/remove", &ServerNodelet::RemoveMissionFromBuffer, this);
    srvBufferControl = nodehandle.advertiseService("buffer/control", &ServerNodelet::BufferControl, this);
    srvMissionBufferList = nodehandle.advertiseService("buffer/list", &ServerNodelet::MissionBufferList, this);

    // Publisher and subscribrer
    missionStatusSub = nodehandle.subscribe<std_msgs::Bool>("mission_server/receive_status", 1000, &ServerNodelet::TreatMissionPlanState, this);

    missionPlanPub = nodehandle.advertise<std_msgs::String>("mission_server/send_plan", 1000, false);
    missionControlPub = nodehandle.advertise<std_msgs::String>("mission_server/send_control", 1000, false);

    missionPlanUpdatePub = nodehandle.advertise<mission_control::MissionListUpdate>("plan/list_update", 1000, true);
    missionSchedUpdatePub = nodehandle.advertise<mission_control::ScheduleListUpdate>("schedule/list_update", 1000, true);
    missionBufferUpdatePub = nodehandle.advertise<mission_control::BufferMissionUpdate>("buffer/list_update", 1000, true);

    bufferStatusUpdatePub = nodehandle.advertise<std_msgs::Bool>("buffer/playing", 1000, true);
    actualMissionUpdatePub = nodehandle.advertise<std_msgs::String>("current_mission/update", 1000, true);

    // Compute schedule execution time
    mutSched.lock();

    for (int index = 0; index < schedPointer->size(); index++)
    {
        time_t execTime = ComputeScheduleRequestTime(schedPointer->at(index));

        if (execTime == -1)
        {
            LOGGER_INFO("The schedule " << schedPointer->at(index).id << " already finished.");

            msched.Remove(schedPointer->at(index).id);

            index--;
        }
        else
            scheduleTimes.push_back(execTime);
    }

    mutSched.unlock();

    playState = false;
    stateMachineStatus = true;

    actualMission.bufferId = -2;
    actualMission.missionPlan.name = "";

    BufferMissionUpdate message;
    message.buffer = missionBuffer;

    StartCycle();
}


void ServerNodelet::TreatMissionPlanState(const std_msgs::Bool::ConstPtr& mp)
{
    std_msgs::Bool message = *mp;
    std_msgs::String message1;
    message1.data = "";

    stateMachineStatus = message.data;
    
    mutBuffer.lock();

    // This topic publishes true if the machine is executing a mission and 
    // false otherwise.
    if (!stateMachineStatus && playState)
        SendMission();
    else if (!stateMachineStatus)
        actualMissionUpdatePub.publish(message1);

    mutBuffer.unlock();
}


bool ServerNodelet::ExecuteSpecificMission(int position)
{
    bool success = true;

    if (actualMission.bufferId >= 0)
    {
        CancelMission(-1, 0);
    }

    if (position != 0)
        success = RearrangeBuffer(position, position, ServerNodelet::ShiftOrder::UP);

    mutBuffer.lock();

    if (success && playState)
        SendMission();
    else if (success && !playState)
    {
        playState = true;
        SendMission();
        playState = false;
    }

    mutBuffer.unlock();

    return success;
}


bool ServerNodelet::RearrangeBuffer(int position, int times, ServerNodelet::ShiftOrder shift)
{
    if (missionBuffer.size() == 0)
    {
        LOGGER_ERROR("There is no mission plans on the buffer.");

        return false;
    }
    else if (position > missionBuffer.size() - 1)
    {
        LOGGER_ERROR("Invalid position on buffer.");

        return false;
    }

    int newPosition;

    BufferMission bufferElement;

    if (shift == UP)
    {
        newPosition = position - times;

        if (newPosition < 0)
        {
            newPosition = 0;

            LOGGER_INFO("New position out of range, moving mission plan to the front.");
        }
        if (position == 0)
        {
            LOGGER_INFO("Mission already at the front of the buffer.");

            return false;
        }
    }
    else if (shift == DOWN)
    {
        newPosition = position + times;

        if (newPosition >= missionBuffer.size())
        {
            newPosition = missionBuffer.size() - 1;

            LOGGER_INFO("New position out of range, moving misson plan to the back.");
        }
        if (position == missionBuffer.size() - 1)
        {
            LOGGER_INFO("Mission already at the back of the buffer.");

            return false;
        }
    }

    mutBuffer.lock();

    bufferElement = missionBuffer.at(position);

    missionBuffer.erase(missionBuffer.begin() + position);
    missionBuffer.insert(missionBuffer.begin() + newPosition, bufferElement);

    BufferMissionUpdate message;
    message.buffer = missionBuffer;

    missionBufferUpdatePub.publish(message);

    mutBuffer.unlock();

    return true;
}


bool ServerNodelet::CancelMission(int position, int id)
{    
    if (position == -1)
    {
        if (actualMission.bufferId < 0)
        {
            LOGGER_ERROR("There is no current mission.");

            return false;
        }

        actualMission.bufferId = -1;

        std_msgs::String message;
        message.data = "Stop";

        missionControlPub.publish(message);

        return true;
    }
    else if (missionBuffer.at(position).bufferId == id)
    {
        mutBuffer.lock();

        missionBuffer.erase(missionBuffer.begin() + position);

        BufferMissionUpdate message;
        message.buffer = missionBuffer;

        missionBufferUpdatePub.publish(message);

        mutBuffer.unlock();

        return true;
    }
    else
    {        
        LOGGER_ERROR("Invalid mission position on buffer.");

        return false;
    }
}


void ServerNodelet::SendMission()
{
    if (missionBuffer.size() != 0 && playState && stateMachineStatus == false)
    {
        actualMission = missionBuffer.at(0);
        missionBuffer.erase(missionBuffer.begin());

        std_msgs::String message;
        message.data = mpserial.Serialize(actualMission.missionPlan);

        missionPlanPub.publish(message);

        BufferMissionUpdate message1;
        message1.buffer = missionBuffer;

        missionBufferUpdatePub.publish(message1);

        std_msgs::String message2;
        message2.data = actualMission.missionPlan.name;

        actualMissionUpdatePub.publish(message2);

        stateMachineStatus = true;
    }
    else if (playState)
    {
        std_msgs::String message3;
        message3.data = "";

        actualMissionUpdatePub.publish(message3);
    }
}


void ServerNodelet::NotifyBufferUpdate(BufferMission missionPlan)
{
    BufferMissionUpdate message;
    message.buffer = missionBuffer;

    missionBufferUpdatePub.publish(message);
}


void ServerNodelet::NotifyScheduleUpdate(ScheduleInfo schedule, int type)
{
    ScheduleListUpdate message;

    message.action = type;
    message.schedule = schedule;

    missionSchedUpdatePub.publish(message);
}


bool ServerNodelet::GetCurrentMission(GetCurrentMission::Request& req, GetCurrentMission::Response& res)
{
    if (actualMission.bufferId >= 0)
        res.mission = actualMission;
    else
    {
        BufferMission bufferMission;
        bufferMission.bufferId = -1;
        bufferMission.missionPlan.name = "";

        res.mission = bufferMission;

        LOGGER_INFO("There is no mission being executed at the moment.");
    }

    return true;
}


bool ServerNodelet::SetCurrentMission(SetCurrentMission::Request& req, SetCurrentMission::Response& res)
{
    res.success = ExecuteSpecificMission(req.position);

    return true;
}


bool ServerNodelet::CancelCurrentMission(CancelCurrentMission::Request& req, CancelCurrentMission::Response& res)
{
    if (actualMission.bufferId >= 0)
    {
        std_msgs::String message;
        message.data = "";

        res.success = CancelMission(-1, actualMission.bufferId);

        if (res.success && playState)
        {
            mutBuffer.lock();
            SendMission();
            mutBuffer.unlock();
        }
        else if (res.success)
            actualMissionUpdatePub.publish(message);
    }
    else
    {
        LOGGER_INFO("There is no mission being executed at the moment.");

        res.success = false;
    }

    return true;
}


bool ServerNodelet::AddMissionToBuffer(AddMissionToBuffer::Request& req, AddMissionToBuffer::Response& res)
{
    int newId = 0;
    res.success = false;

    auto it = missionBuffer.begin();

    mutBuffer.lock();

    if (req.position > missionBuffer.size() - 1)
        it = missionBuffer.end();
    else
        it += req.position;

    BufferMission newMission;

    for (int i = 0; i < missionBuffer.size(); i++)
    {
        if (newId <= missionBuffer.at(i).bufferId)
            newId = missionBuffer.at(i).bufferId + 1;
    }

    for (int i = 0; i < mpPointer->size(); i++)
    {
        if (mpPointer->at(i).name == req.name)
        {
            newMission.bufferId = newId;
            newMission.missionPlan = mpPointer->at(i);

            if (missionBuffer.size() == 0 && !stateMachineStatus)
            {
                missionBuffer.push_back(newMission);
                SendMission();

                // False or true?
                res.success = true;

                //mutBuffer.unlock();

                //return true;
            }
            else
                missionBuffer.insert(it, newMission);

            res.success = true;
        }
    }

    BufferMissionUpdate message;
    message.buffer = missionBuffer;

    if (res.success)
        missionBufferUpdatePub.publish(message);
    else
        LOGGER_INFO("Mission plan doesn't exist.");

    mutBuffer.unlock();

    return true;
}


bool ServerNodelet::MoveMissionOnBuffer(MoveMissionOnBuffer::Request& req, MoveMissionOnBuffer::Response& res)
{
    if (req.shift)
        res.success = RearrangeBuffer(req.position, req.times, ServerNodelet::ShiftOrder::UP);
    else
        res.success = RearrangeBuffer(req.position, req.times, ServerNodelet::ShiftOrder::DOWN);

    return true;
}


bool ServerNodelet::RemoveMissionFromBuffer(RemoveMissionFromBuffer::Request& req, RemoveMissionFromBuffer::Response& res)
{
    res.success = CancelMission(req.pos, req.id);

    return true;
}


bool ServerNodelet::BufferControl(BufferControl::Request& req, BufferControl::Response& res)
{
    std_msgs::String message;
    std_msgs::Bool message1;

    switch (req.Flag)
    {
    case Play:
        message.data = "Play";
        //missionControlPub.publish(message);
        playState = true;
        SendMission();
        message1.data = true;
        bufferStatusUpdatePub.publish(message1);
        res.success = true;
        break;
    case Pause:
        playState = false;
        message1.data = false;
        bufferStatusUpdatePub.publish(message1);
        res.success = true;
        break;
    case Stop:
        message.data = "Stop";
        missionControlPub.publish(message);
        playState = false;
        message1.data = false;
        bufferStatusUpdatePub.publish(message1);
        res.success = true;
        CancelMission(-1, 0);
        break;
    default:
        res.success = false;
        LOGGER_ERROR("Invalid control value.");
        break;
    }

    return true;
}


bool ServerNodelet::MissionBufferList(MissionBufferList::Request& req, MissionBufferList::Response& res)
{
    vector<BufferMission> bufferList;

    for (auto i = 0; i < missionBuffer.size(); i++)
        bufferList.push_back(missionBuffer.at(i));

    res.missionBuffer = bufferList;

    return true;
}


bool ServerNodelet::AddMissionPlan(AddMissionPlan::Request& req, AddMissionPlan::Response& res)
{
    for (int i = 0; i < mpPointer->size(); i++)
    {
        if (mpPointer->at(i).name == req.missionPlan.name)
        {
            res.success = false;
            LOGGER_INFO("Mission plan already exists.");            
            
            return true;
        }
    }

    mutPlan.lock();

    mpman.AddMissionPlan(req, res);

    mutPlan.unlock();

    MissionListUpdate message;
    message.action = Add;
    message.mission = req.missionPlan;

    missionPlanUpdatePub.publish(message);

    return true;
}


bool ServerNodelet::RemoveMissionPlan(RemoveMissionPlan::Request& req, RemoveMissionPlan::Response& res)
{
    bool success = false;

    mutBuffer.lock();

    // Remove all missions from buffer
    for (int i = 0; i < missionBuffer.size(); i++)
    {
        if (missionBuffer.at(i).missionPlan.name == req.name)
        {
            success = CancelMission(i, missionBuffer.at(i).bufferId);
            cout << "Deletei!" << endl;
        }
    }

    mutBuffer.unlock();

    // Stops actual mission and start the next one
    if (actualMission.missionPlan.name == req.name)
    {
        success = CancelMission(-1, 0);

        cout << "Parei!" << endl;

        if (playState)
            SendMission();
    }

    // Remove all schedules that contains the mission and their times
    mutSched.lock();

    for (int i = 0; i < schedPointer->size(); i++)
    {
        if (schedPointer->at(i).missionPlanName == req.name)
        {
            ScheduleListUpdate message;
            message.action = Remove;
            message.schedule = schedPointer->at(i);

            missionSchedUpdatePub.publish(message);

            scheduleTimes.erase(scheduleTimes.begin() + i);
            msched.Remove(schedPointer->at(i).id);
        }
    }

    mutSched.unlock();

    mutPlan.lock();

    // Get the mission for the message
    MissionPlan mission;

    for (int i = 0; i < mpPointer->size(); i++)
    {
        if (mpPointer->at(i).name == req.name)
        {
            mission = mpPointer->at(i);

            break;
        }
    }

    mpman.RemoveMissionPlan(req, res);

    mutPlan.unlock();

    if (res.success)
    {
        MissionListUpdate message;
        message.action = Remove;
        message.mission = mission;

        missionPlanUpdatePub.publish(message);
    }
    else
        LOGGER_INFO("Mission plan doesn't exist.");

    cond.notify_one();

    return true;
}


bool ServerNodelet::MissionPlanList(MissionPlanList::Request& req, MissionPlanList::Response& res)
{
    return mpman.MissionPlanList(req, res);
}


/*
 * \TODO Fix error message for when the mission time is not valid. It shouldn't 
 *       print 'Invalid schedule type'. The error messsage is misleading. 
 */
bool ServerNodelet::AddMissionPlanSchedule(AddMissionPlanSchedule::Request& req, AddMissionPlanSchedule::Response& res)
{   
    mutSched.lock();

    // Compute new schedule execution time
    time_t execTime = ComputeScheduleRequestTime(req.schedule);

    if (execTime == -1)
    {
        LOGGER_ERROR("Invalid schedule type.");
        res.success = false;

        mutSched.unlock();

        return true;
    }

    scheduleTimes.push_back(execTime);
    msched.AddMissionPlanSchedule(req, res);

    mutSched.unlock();

    ScheduleListUpdate message;
    message.action = Add;
    message.schedule = req.schedule;

    missionSchedUpdatePub.publish(message);

    cond.notify_one();

    return true;
}


bool ServerNodelet::RemoveMissionPlanSchedule(RemoveMissionPlanSchedule::Request& req, RemoveMissionPlanSchedule::Response& res)
{
    // Remove schedule execution time from list
    mutSched.lock();

    for (int i = 0; i < schedPointer->size(); i++)
    {
        if (schedPointer->at(i).id == req.Id)
        {
            ScheduleListUpdate message;

            message.action = Remove;
            message.schedule = schedPointer->at(i);

            missionSchedUpdatePub.publish(message);

            scheduleTimes.erase(scheduleTimes.begin() + i);
        }
    }

    msched.RemoveMissionPlanSchedule(req, res);

    mutSched.unlock();

    if (!res.success)
        LOGGER_INFO("Schedule doesn't exist.");

    cond.notify_one();

    return true;
}


bool ServerNodelet::ScheduleList(ScheduleList::Request& req, ScheduleList::Response& res)
{
    return msched.ScheduleList(req, res);
}


ServerNodelet::~ServerNodelet()
{
    StopCycle();

    LOGGER_INFO("mission_control::Server: Saving mission plan list to file " <<
            missionPath << ".");

    mutPlan.lock();

    mpman.Save(missionPath.c_str());

    mutPlan.unlock();

    LOGGER_INFO("mission_control::Server: Saving schedule list to file " <<
            schedulePath << ".");

    mutSched.lock();

    msched.Save(schedulePath.c_str());

    mutSched.unlock();

    // Shutdown ROS services
    srvAddMissionPlan.shutdown();
    srvRemoveMissionPlan.shutdown();
    srvMissionPlanList.shutdown();
    srvAddMissionPlanSchedule.shutdown();
    srvRemoveMissionPlanSchedule.shutdown();
    srvScheduleList.shutdown();
    srvGetCurrentMission.shutdown();
    srvSetCurrentMission.shutdown();
    srvCancelCurrentMission.shutdown();
    srvAddMissionToBuffer.shutdown();
    srvMoveMissionOnBuffer.shutdown();
    srvRemoveMissionFromBuffer.shutdown();
    srvBufferControl.shutdown();
    srvMissionBufferList.shutdown();
}

// mission_plan_manager.cpp




#include <mission_control/mission_plan_manager.h>

#include <iostream>
#include <vector>

#include <simple_xml_settings/simple_xml_settings.h>



using namespace std;
using namespace mission_control;



MissionPlanManager::MissionPlanManager()
{
}


MissionPlanManager::~MissionPlanManager()
{
}
    

int MissionPlanManager::Save(const char* filename)
{
    SimpleXMLSettings xmlSettings(filename, "missionPlan");

    xmlSettings.BeginSave();

    // Save each mission plan
    for (int i = 0; i < missionPlan.size(); i++)
    {
        xmlSettings.BeginGroup("MissionPlan", i);

        xmlSettings.SaveText("name", missionPlan[i].name);

        // Save each mission
        for (int j = 0; j < missionPlan[i].missions.size(); j++)
        {
            xmlSettings.BeginGroup("Mission", j);

            xmlSettings.SaveText("name", missionPlan[i].missions[j].name);

            // Save each param
            for (int k = 0; k < missionPlan[i].missions[j].parameterKey.size(); k++)
            {
                xmlSettings.BeginGroup("Param", k);
                xmlSettings.SaveText("name", missionPlan[i].missions[j].parameterKey[k]);
                xmlSettings.SaveText("value", missionPlan[i].missions[j].parameterValue[k]);
                xmlSettings.EndGroup();
            }
            xmlSettings.EndGroup();
        }
        xmlSettings.EndGroup();
    }

    xmlSettings.EndSave();

    return 0;
}


int MissionPlanManager::Load(const char* filename)
{
    SimpleXMLSettings xmlSettings(filename, "missionPlan");

    xmlSettings.BeginLoad();

    int planIndex = 0;

    // Load all mission plans
    while (xmlSettings.BeginGroup("MissionPlan", planIndex))
    {
        MissionPlan plan;

        plan.name   = xmlSettings.LoadText("name", "unnamed plan");

        int missionIndex = 0;

        // Load all missions
        while (xmlSettings.BeginGroup("Mission", missionIndex))
        {
            Mission mission;
            mission.parameterKey.clear();
            mission.parameterValue.clear();

            mission.name = xmlSettings.LoadText("name", "");

            int paramIndex = 0;

            // Load all params
            while (xmlSettings.BeginGroup("Param", paramIndex))
            {
                mission.parameterKey.push_back(xmlSettings.LoadText("name", ""));
                mission.parameterValue.push_back(xmlSettings.LoadText("value", ""));

                paramIndex++;
                xmlSettings.EndGroup();
            }

            plan.missions.push_back(mission);

            missionIndex++;
            xmlSettings.EndGroup();
        }

        Add(plan);

        planIndex++;
        xmlSettings.EndGroup();
    }

    xmlSettings.EndLoad();

    return 0;
}


int MissionPlanManager::Add(MissionPlan& plan)
{
    missionPlan.push_back(plan);

    return 1;
}


int MissionPlanManager::Remove(const string name)
{
    for (int i = 0; i < missionPlan.size(); i++)
    {
        if (missionPlan[i].name == name) {
            missionPlan.erase(missionPlan.begin() + i);

            return 1;
        }
    }

    return 0;
}


const std::vector<MissionPlan>* MissionPlanManager::GetMissionPlanList()
{
    return &missionPlan;
}


bool MissionPlanManager::AddMissionPlan(AddMissionPlan::Request& req, AddMissionPlan::Response& res)
{
    int ret = Add(req.missionPlan);
    res.success = ret == 1;

    return true;
}


bool MissionPlanManager::RemoveMissionPlan(RemoveMissionPlan::Request& req, RemoveMissionPlan::Response& res)
{
    int ret = Remove(req.name);
    res.success = ret == 1;

    return true;
}


bool MissionPlanManager::MissionPlanList(MissionPlanList::Request& req, MissionPlanList::Response& res)
{
    auto it_end = missionPlan.end();

    for (auto it = missionPlan.begin(); it != it_end; it++)
    {
        res.missionPlanList.push_back(*it);
    }

    return true;
}

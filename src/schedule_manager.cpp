// schedule_manager.cpp



#include <mission_control/schedule_manager.h>

#include <iostream>
#include <string>
#include <vector>

#include <simple_xml_settings/simple_xml_settings.h>



using namespace std;
using namespace mission_control;



ScheduleManager::ScheduleManager() : scheduleList(0)
{    
}


ScheduleManager::~ScheduleManager()
{
}


int ScheduleManager::Save(const char* filename)
{
    SimpleXMLSettings xml(filename, "ScheduleList");

    xml.BeginSave();

    for (int i = 0; i < scheduleList.size(); i++)
    {
        xml.BeginGroup("ScheduleInfo", i);
        xml.SaveNumber("Id", scheduleList[i].id);
        xml.SaveText("MissionPlanName", scheduleList[i].missionPlanName);
        xml.SaveNumber<int>("TimeOfDay", scheduleList[i].timeOfDay.toSec());
        xml.SaveNumber("ScheduleType", scheduleList[i].scheduleType);
        xml.SaveNumber<int>("TimeDelay", scheduleList[i].timeDelay.toSec());

        xml.BeginGroup("StartDate");
        xml.SaveNumber("Day", scheduleList[i].startDate.day);
        xml.SaveNumber("Month", scheduleList[i].startDate.month);
        xml.SaveNumber("Year", scheduleList[i].startDate.year);
        xml.EndGroup();

        xml.BeginGroup("EndDate");
        xml.SaveNumber("Day", scheduleList[i].endDate.day);
        xml.SaveNumber("Month", scheduleList[i].endDate.month);
        xml.SaveNumber("Year", scheduleList[i].endDate.year);
        xml.EndGroup();

        xml.BeginGroup("Days");
        xml.SaveBool("Sunday", scheduleList[i].days[0]);
        xml.SaveBool("Monday", scheduleList[i].days[1]);
        xml.SaveBool("Tuesday", scheduleList[i].days[2]);
        xml.SaveBool("Wednesday", scheduleList[i].days[3]);
        xml.SaveBool("Thursday", scheduleList[i].days[4]);
        xml.SaveBool("Friday", scheduleList[i].days[5]);
        xml.SaveBool("Saturday", scheduleList[i].days[6]);
        xml.EndGroup();

        xml.EndGroup();
    }

    xml.EndSave();

    return 0;
}


int ScheduleManager::Load(const char* filename)
{
    SimpleXMLSettings xml(filename, "ScheduleList");

    xml.BeginLoad();

    int scheduleIndex = 0;

    while (xml.BeginGroup("ScheduleInfo", scheduleIndex))
    {
        ScheduleInfo newSchedule;

        newSchedule.id = xml.LoadNumber("Id", -1);
        newSchedule.missionPlanName = xml.LoadText("MissionPlanName", "");
        newSchedule.timeOfDay.fromSec(xml.LoadNumber<int>("TimeOfDay", 0));
        newSchedule.scheduleType = xml.LoadNumber("ScheduleType", 0);
        newSchedule.timeDelay.fromSec(xml.LoadNumber<int>("TimeDelay", 0));

        if (xml.BeginGroup("StartDate"))
        {
            newSchedule.startDate.day = xml.LoadNumber("Day", 0);
            newSchedule.startDate.month = xml.LoadNumber("Month", 0);
            newSchedule.startDate.year = xml.LoadNumber("Year", 0);

            xml.EndGroup();
        }

        if (xml.BeginGroup("EndDate"))
        {
            newSchedule.endDate.day = xml.LoadNumber("Day", 0);
            newSchedule.endDate.month = xml.LoadNumber("Month", 0);
            newSchedule.endDate.year = xml.LoadNumber("Year", 0);

            xml.EndGroup();
        }

        if (xml.BeginGroup("Days"))
        {
            newSchedule.days[0] = xml.LoadBool("Sunday", false);
            newSchedule.days[1] = xml.LoadBool("Monday", false);
            newSchedule.days[2] = xml.LoadBool("Tuesday", false);
            newSchedule.days[3] = xml.LoadBool("Wednesday", false);
            newSchedule.days[4] = xml.LoadBool("Thursday", false);
            newSchedule.days[5] = xml.LoadBool("Friday", false);
            newSchedule.days[6] = xml.LoadBool("Saturday", false);

            xml.EndGroup();
        }

        Add(newSchedule);

        scheduleIndex++;

        xml.EndGroup();
    }

    xml.EndLoad();

    return 0;
}


int ScheduleManager::Add(ScheduleInfo& schedule)
{
    if (scheduleList.size() != 0 && schedule.id <= scheduleList.back().id)
    {
        schedule.id = scheduleList.back().id + 1;
    }

    scheduleList.push_back(schedule);

    return 1;
}


int ScheduleManager::Remove(const int id)
{
    for (int i = 0; i < scheduleList.size(); i++)
    {
        if (scheduleList[i].id == id)
        {
            scheduleList.erase(scheduleList.begin() + i);

            return 1;
        }
    }

    return 0;
} 


const vector<ScheduleInfo>* ScheduleManager::GetScheduleList()
{
    return &scheduleList;
}


bool ScheduleManager::AddMissionPlanSchedule(AddMissionPlanSchedule::Request& req, AddMissionPlanSchedule::Response& res)
{
    int ret = Add(req.schedule);
    res.success = ret == 1;

    return true;
}


bool ScheduleManager::RemoveMissionPlanSchedule(RemoveMissionPlanSchedule::Request& req, RemoveMissionPlanSchedule::Response& res)
{
    int ret = Remove(req.Id);
    res.success = ret == 1;

    return true;
}


bool ScheduleManager::ScheduleList(ScheduleList::Request& req, ScheduleList::Response& res)
{
    auto it_end = scheduleList.end();

    for (auto it = scheduleList.begin(); it != it_end; it++)
    {
        res.scheduleList.push_back(*it);
    }

    return true;
}

// MissionPlanSerialization.cpp



#include <mission_control/mission_plan_serialization.h>

#include <mission_control/Mission.h>

#include <iostream>
#include <string>
#include <cstring>


using namespace std;
using namespace mission_control;



void MissionPlanSerialization::AddDefinition(const char* missionName, int missionId, const char* paramName, PARAMTYPE type)
{
    ParamTypeList& def = missionDefinitions[string(missionName)];

    def.id = missionId;
    string param = paramName;
    def.params.push_back(param);
    def.types.push_back(type);
}


MissionPlanSerialization::MissionPlanSerialization()
{
    AddDefinition("Goto", 0, "velocity_mode",   PARAMTYPE::String);
    AddDefinition("Goto", 0, "position_togo",   PARAMTYPE::Float);
    AddDefinition("Goto", 0, "way",             PARAMTYPE::Int);
    AddDefinition("Goto", 0, "rounds",          PARAMTYPE::Int);
    AddDefinition("Goto", 0, "circular",        PARAMTYPE::Bool);
}


MissionPlanSerialization::~MissionPlanSerialization()
{
}


string MissionPlanSerialization::Serialize(const MissionPlan& mp)
{
    string serialized = "[";
    // Loop through each mission
    for (auto it = mp.missions.begin(); it != mp.missions.end(); it++)
    {
        Mission mission = *it;
        
        // Create a map to hold the mission parameters
        ParamMap missionParamMap;
        
        for (int i = 0; i < mission.parameterKey.size(); i++)
        {
            missionParamMap[mission.parameterKey[i]] = mission.parameterValue[i];
        }
        
        ParamTypeList missionParams = missionDefinitions[mission.name];

        serialized += "[";
        
        serialized += to_string(missionParams.id);
        serialized += ", ";

        for (int i = 0; i < missionParams.params.size(); i++)
        {
            switch (missionParams.types[i])
            {
            case Float:
                serialized += missionParamMap[missionParams.params[i]];
                break;
            case Int:
                serialized += missionParamMap[missionParams.params[i]];
                break;
            case Bool:
                serialized += missionParamMap[missionParams.params[i]];
                break;
            case String:
                serialized += "\"";
                serialized += missionParamMap[missionParams.params[i]];
                serialized += "\"";
                break;
            default:
                break;
            }
            
            serialized += ", ";
        }
        
        serialized += "], ";
    }
    
    serialized += "]";
    
    return serialized;
}


// server.cpp
 
 
 
#include <mission_control/server.h>

#include <string>
#include <ratio>

 
 
using namespace mission_control;
using namespace std;

 
 
Server::Server()
{
}


time_t Server::ComputeScheduleRequestTime(const ScheduleInfo& schedule)
{
    switch (schedule.scheduleType)
    {
    // Treat one-shot schedules.
    case SingleSchedule:
        return ComputeSingleScheduleRequestTime(schedule);
    // Treat timed schedules.
    case TimedSchedule:
        return ComputeTimedScheduleRequestTime(schedule);
    // Treat recurring schedules.
    case RecurringSchedule:
        return ComputeRecurringScheduleRequestTime(schedule);
    default:
        return 0;
    }
}


time_t Server::ComputeSingleScheduleRequestTime(const ScheduleInfo& schedule)
{
    // Get current time
    time_t timeraw = time(NULL);
    struct tm* currTime = localtime(&timeraw);

    struct tm t;
    t.tm_year  = schedule.startDate.year - 1900;
    t.tm_mon   = schedule.startDate.month - 1;
    t.tm_mday  = schedule.startDate.day;

    t.tm_sec   = (int)round(schedule.timeOfDay.toSec());
    t.tm_min   = 0;
    t.tm_hour  = 0;

    t.tm_isdst = currTime->tm_isdst;

    if (difftime(mktime(&t), mktime(currTime)) < 0)
        return -1;
    else
        return mktime(&t);
}


time_t Server::ComputeTimedScheduleRequestTime(const ScheduleInfo& schedule)
{
    // Get current time
    time_t timeraw = time(NULL);
    struct tm* currTime = localtime(&timeraw);

    struct tm startTime;
    startTime.tm_year = schedule.startDate.year - 1900;
    startTime.tm_mon  = schedule.startDate.month - 1;
    startTime.tm_mday = schedule.startDate.day;

    startTime.tm_sec  = schedule.timeOfDay.toSec();
    startTime.tm_min  = 0;
    startTime.tm_hour = 0;

    startTime.tm_isdst = currTime->tm_isdst;

    time_t timeDifference = difftime(mktime(currTime), mktime(&startTime));

    if (timeDifference < 0)
    {
        return mktime(&startTime);
    }
    else
    {
        struct tm endDate;
        endDate.tm_year  = schedule.endDate.year - 1900;
        endDate.tm_mon   = schedule.endDate.month - 1;
        endDate.tm_mday  = schedule.endDate.day + 1;

        endDate.tm_sec   = 0;
        endDate.tm_min   = 0;
        endDate.tm_hour  = 0;

        endDate.tm_isdst = currTime->tm_isdst;

        int periodCount = floor(timeDifference / schedule.timeDelay.toSec());

        startTime.tm_sec += (periodCount + 1) * schedule.timeDelay.toSec();

        if (difftime(mktime(&endDate), mktime(&startTime)) < 0)
            return -1;
        else
            return mktime(&startTime);
    }
}


time_t Server::ComputeRecurringScheduleRequestTime(const ScheduleInfo& schedule)
{
    // Get current time
    time_t timeraw = time(NULL);
    struct tm* currTime = localtime(&timeraw);

    struct tm endDate;
    endDate.tm_year  = schedule.endDate.year - 1900;
    endDate.tm_mon   = schedule.endDate.month - 1;
    endDate.tm_mday  = schedule.endDate.day + 1;

    endDate.tm_sec   = 0;
    endDate.tm_min   = 0;
    endDate.tm_hour  = 0;

    endDate.tm_isdst = currTime->tm_isdst;

    struct tm requestTime;

    // Check if today is valid
    if (schedule.days[currTime->tm_wday] == true)
    {
        // Check if hour and minute are valid
        if (schedule.timeOfDay.toSec() > currTime->tm_hour*3600 + currTime->tm_min*60 + currTime->tm_sec)
        {
            requestTime.tm_year  = currTime->tm_year;
            requestTime.tm_mon   = currTime->tm_mon;
            requestTime.tm_mday  = currTime->tm_mday;

            requestTime.tm_sec   = schedule.timeOfDay.toSec();
            requestTime.tm_min   = 0;
            requestTime.tm_hour  = 0;

            requestTime.tm_isdst = currTime->tm_isdst;

            if (difftime(mktime(&endDate), mktime(&requestTime)) < 0)
                return -1;
            else
                return mktime(&requestTime);
        }
    }

   // Check how many days until next valid day
    int nextDays = 0;

    for (int i = currTime->tm_wday + 1; i <= 6; i++)
    {
        if (schedule.days[i] == true)
        {
            nextDays = i - currTime->tm_wday;
            break;
        }
    }

    // If we didn't found any, check next week
    if (nextDays == 0)
    {
        nextDays = 6 - currTime->tm_wday + 1;

        for (int i = 0; i < currTime->tm_wday; i++)
        {
            if (schedule.days[i] == true)
            {
                break;
            }
            else
            {
                nextDays++;
            }
        }
    }

    requestTime.tm_year  = currTime->tm_year;
    requestTime.tm_mon   = currTime->tm_mon;
    requestTime.tm_mday  = currTime->tm_mday + nextDays;

    requestTime.tm_sec   = schedule.timeOfDay.toSec();
    requestTime.tm_min   = 0;
    requestTime.tm_hour  = 0;

    requestTime.tm_isdst = currTime->tm_isdst;

    if (difftime(mktime(&endDate), mktime(&requestTime)) < 0)
        return -1;
    else
        return mktime(&requestTime);
}


int Server::AddNextSchedule()
{
    time_t actualTime = time(NULL);

    for (int i = 0; i < scheduleTimes.size(); i++)
    {
        if (scheduleTimes.at(i) - actualTime <= 0)
        {     
            // Add mission plans from schedule to the buffer
            switch (schedPointer->at(i).scheduleType)
            {
            case SingleSchedule:
                return AddSingleSchedule(i);
            case TimedSchedule:
            case RecurringSchedule:
                return AddTimedSchedule(i);
            }
        }
    }

    return -1;
}


// When a new schedule is added by calling the service sometimes there is a delay
int Server::AddSingleSchedule(int position)
{
    mutSched.lock();
    mutBuffer.lock();

    int type = 1;

    int newId = 0;
    BufferMission newMission;

    for (int i = 0; i < missionBuffer.size(); i++)
    {
        if (newId <= missionBuffer.at(i).bufferId)
            newId = missionBuffer.back().bufferId + 1;
    }

    string planName = schedPointer->at(position).missionPlanName;

    for (int i = 0; i < mpPointer->size(); i++)
    {
        if (mpPointer->at(i).name == planName)
        {
            newMission.bufferId = newId;
            newMission.missionPlan = mpPointer->at(i);

            missionBuffer.push_back(newMission);

            NotifyBufferUpdate(newMission);

            break;
        }
    }

    mutBuffer.unlock();

    NotifyScheduleUpdate(schedPointer->at(position), type);

    scheduleTimes.erase(scheduleTimes.begin() + position);
    msched.Remove(schedPointer->at(position).id);

    mutSched.unlock();

    return 0;
}


int Server::AddTimedSchedule(int position)
{
    mutSched.lock();
    mutBuffer.lock();

    int type = 0;

    int newId = 0;
    BufferMission newMission;

    for (int i = 0; i < missionBuffer.size(); i++)
    {
        if (newId <= missionBuffer.at(i).bufferId)
            newId = missionBuffer.back().bufferId + 1;
    }

    string planName = schedPointer->at(position).missionPlanName;

    for (int i = 0; i < mpPointer->size(); i++)
    {
        if (mpPointer->at(i).name == planName)
        {
            newMission.bufferId = newId;
            newMission.missionPlan = mpPointer->at(i);

            missionBuffer.push_back(newMission);

            NotifyBufferUpdate(newMission);

            break;
        }
    }

    mutBuffer.unlock();

    time_t execTime = ComputeScheduleRequestTime(schedPointer->at(position));

    if (execTime == -1)
    {
        NotifyScheduleUpdate(schedPointer->at(position), type + 1);

        scheduleTimes.erase(scheduleTimes.begin() + position);
        msched.Remove(schedPointer->at(position).id);

        mutSched.unlock();

        return 0;
    }

    scheduleTimes.at(position) = execTime;

    mutSched.unlock();

    return 1;
}


void Server::Cycle(Server* server)
{
    server->UpdateBuffer();
}


void Server::UpdateBuffer()
{
    boost::unique_lock<boost::mutex> lock(mut);
    while (cycleOn)
    {
        int index = 0;

        time_t nextTime = 0;
        time_t actualTime = time(NULL);

        if (scheduleTimes.size() != 0)
        {
            AddNextSchedule();

            if (missionBuffer.size() == 1 && !stateMachineStatus)
                SendMission();

            if (scheduleTimes.size() == 0)
                break;

            nextTime = actualTime;
            for (int i = 0; i < scheduleTimes.size(); i++)
            {
                if (scheduleTimes.at(i) - actualTime < nextTime)
                {
                    nextTime = scheduleTimes.at(i) - actualTime;
                    index = i;
                }
                cout << "Time " << i << ": " << scheduleTimes.at(i) - actualTime << endl;
            }

            if (schedPointer->size() > 0)
            {
                struct tm* timesss = localtime(&scheduleTimes.at(index));
                cout << schedPointer->at(index).missionPlanName << endl;
                cout << "Vou dormir por: " << asctime(timesss) << endl;

                for (int i = 0; i < schedPointer->size(); i++)
                    cout << "Tipo: "  << schedPointer->at(i).scheduleType << endl;
            }

        }

        boost::posix_time::ptime time = boost::posix_time::from_time_t(nextTime);
        boost::posix_time::time_duration countdown = time.time_of_day();

        if (countdown.total_milliseconds() > 0)
        {
            cond.timed_wait(lock, countdown);
            cout << "Acordei!" << endl;
        }
    }
}


void Server::StartCycle()
{
    // Initializes thread
    cycleThread = new boost::thread(Server::Cycle, this);

    cycleOn = true;
}


void Server::StopCycle()
{
    cycleOn = false;
 
    cycleThread->join();   
}


Server::~Server()
{
}
